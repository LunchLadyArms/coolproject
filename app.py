from flask import Flask, request , send_from_directory
from flask_cors import CORS

app = Flask(__name__)
CORS(app)  # This enables CORS for all routes and origins

@app.route('/save-data', methods=['POST'])
def save_data():
    data = request.form['data']
    with open('data.txt', 'a') as file:
        file.write(data + '\n')
    return 'Data saved successfully', 200

@app.route('/')
def index():
    return send_from_directory("static", 'tricky.html')

if __name__ == '__main__':
    app.run(debug=True)